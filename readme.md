# How to contribute
Fork the project and submit a pull request. 


## First time setup
1. Click the "Fork" button in the top left of the navigation. 
You find it under _Action_ or _"..."_ depending on screen size. 
2. In your terminal, clone the repository to your own machine. Change the command to have your own Bitbucket username in the command.
```
git clone git@bitbucket.org:<USERNAME>/rocket-league-advanced.git   
cd rocket-league-advanced/   
git remote add upstream git@bitbucket.org:JoeProBruh/rocket-league-advanced.git  
```

## Adding a new submission
1. Create a new submission branch:
```
git checkout -b submission/<YOUR-SUBMISSION-NAME>
```
2. Commit and push your changes to your fork. (`git push origin submission/<YOUR-SUBMISSION-NAME>`)
3. Go to **your fork** on Bitbucket and click "Create pull request" under _Action_. Select `JoeProBruh/pull_request_submissions` as your target, fill out a description and click "Create Pull Request" when done.
